export enum TabAction {
    default,
    main,
    data,
    mqmd,
    rfh,
    pubsub,
    pscr,
    jms,
    usr,
    other,
    cics,
    ims,
    dlq
}