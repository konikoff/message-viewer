import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MqmdComponent } from './mqmd.component';

describe('MqmdComponent', () => {
  let component: MqmdComponent;
  let fixture: ComponentFixture<MqmdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MqmdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MqmdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
