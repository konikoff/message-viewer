import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfhComponent } from './rfh.component';

describe('RfhComponent', () => {
  let component: RfhComponent;
  let fixture: ComponentFixture<RfhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
