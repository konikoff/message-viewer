import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';

import { MainComponent } from '../ibm-mq/main/main.component';
import { DataComponent } from './data/data.component';
import { ImsComponent } from './ims/ims.component';
import { MqmdComponent } from './mqmd/mqmd.component';
import { CicsComponent } from './cics/cics.component';
import { DlqComponent } from './dlq/dlq.component';
import { OtherComponent } from './other/other.component';
import { PubsubComponent } from './pubsub/pubsub.component';
import { RfhComponent } from './rfh/rfh.component';
import { UsrComponent } from './usr/usr.component';
import { IbmMqComponent } from './ibm-mq.component';
import { PscrComponent } from './pscr/pscr.component';
import { JmsComponent } from './jms/jms.component';


@NgModule({
    declarations: [
        IbmMqComponent,
        MainComponent,
        CicsComponent,
        DataComponent,
        DlqComponent,
        ImsComponent,
        MqmdComponent,
        OtherComponent,
        PubsubComponent,
        RfhComponent,
        UsrComponent,
        PscrComponent,
        JmsComponent
    ],
    imports: [
        CommonModule,
        SharedModule
    ]
})
export class IbmMqModule {
}