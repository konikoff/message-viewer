import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PscrComponent } from './pscr.component';

describe('PscrComponent', () => {
  let component: PscrComponent;
  let fixture: ComponentFixture<PscrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PscrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PscrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
