import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IbmMqComponent } from './ibm-mq.component';

describe('IbmMqComponent', () => {
  let component: IbmMqComponent;
  let fixture: ComponentFixture<IbmMqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IbmMqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IbmMqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
