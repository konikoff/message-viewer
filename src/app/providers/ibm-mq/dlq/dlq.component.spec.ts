import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DlqComponent } from './dlq.component';

describe('DlqComponent', () => {
  let component: DlqComponent;
  let fixture: ComponentFixture<DlqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DlqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DlqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
