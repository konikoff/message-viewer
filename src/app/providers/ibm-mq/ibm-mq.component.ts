import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TabAction } from '../shared/tab.action';

@Component({
  selector: 'app-ibm-mq',
  templateUrl: './ibm-mq.component.html',
  styleUrls: ['./ibm-mq.component.scss']
})
export class IbmMqComponent implements OnInit {

  public tabAction = TabAction;
  public action: TabAction = TabAction.main;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
  }

  onTab(action: TabAction) {
    this.action = action;
    console.log('Action: ' + action.toString());
  }

}
