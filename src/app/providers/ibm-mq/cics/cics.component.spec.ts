import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CicsComponent } from './cics.component';

describe('CicsComponent', () => {
  let component: CicsComponent;
  let fixture: ComponentFixture<CicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
