import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { IbmMqComponent } from 'app/providers/ibm-mq/ibm-mq.component';

const headerRoutes: Routes = [
    {
        path: 'providers/ibm-mq', component: IbmMqComponent
    }
];

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forRoot(headerRoutes)
    ],
    exports: [RouterModule]
})
export class HeaderRoutingModule { }